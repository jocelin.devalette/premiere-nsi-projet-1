# Jeu de Cartes
## Régles du jeu
!!! info "Déroulement de la partie"
    <img class="right" src="carte.png" width="225" height="150"  alt="" title="" style="float:right;margin:0 10px 0 20px;" />

    - On dispose d'un jeu de 24 cartes de couleur vertes, violettes ou orange sur lesquelles est porté un nombre.
    - Distribuer aléatoirement 12 cartes à chaque joueur.
    - Simultanément chaque joueur joue une carte, la carte la plus forte emporte le le pli. Le jeu se joue donc en 12 plis.
    - Quand toutes les cartes sont jouées, chaque joueur fait la somme des nombres portés par les cartes, le plus fort total l'emporte.

!!! info "Valeur des cartes"
    - Les cartes oranges l'emportent sur les cartes vertes et violettes.
    - Les cartes vertes l'emportent sur les cartes violettes.
    - Si deux cartes violettes sont jouées en même temps la plus grande l'emporte.
    - Si deux cartes vertes sont jouées en même temps la plus grande l'emporte
    - Si deux cartes identiques sont jouées en même temps, les cartes sont écartées et ne compteront pas dans le total final.

## Création du jeu de cartes

!!! tldr "la fonction `crea_jeu()`"
    Le jeu de cartes est généré par une fonction `crea_jeu()` qui génère un jeu de 24 cartes grâce au code ci-dessous :
    ```python
    def crea_jeu ( ) :
        jeu = []
        oranges = [0 for i in range(4)]
        vertes = [i for i in range(1, 11)]
        violettes = [ i*10 for i in vertes if i%2 != 0]
        violettes = violettes * 2
        for i in vertes :
            jeu.append((1, i))
        for i in violettes :
            jeu.append((0, i))
        for i in oranges :
            jeu.append((2, i))
        return jeu
    ```

## Etude du jeu de cartes

!!! note "Répondez aux questions"
    Afin de mieux comprendre comment est construit le jeu de cartes, essayez de répondre aux questions suivantes en étudiant le script `crea_jeu()` :

    !!! faq "Question 1" 
        === "Question"
            De Combien de cartes de chacune des trois couleurs est composé le jeu ?

        === "Solution"
            - Couleur Orange : 4 cartes (`for i in range(4)`)
            - Couleur Verte : 10 cartes (`for i in range(1, 11)`)
            - Couleur Orange : 10 cartes (`for i in vertes if i%2 != 0` et `violettes = violettes * 2`)

    !!! faq "Question 2" 
        === "Question"
            Quelle est la valeur des cartes oranges ?

        === "Solution"
            Les cartes oranges ont toutes pour valeur 0 (`[0 for i in range(4)]`)
    
    !!! faq "Question 3" 
        === "Question"
            Quelles sont les valeurs des cartes vertes ?

        === "Solution"
            Les cartes 10 cartes vertes ont des valeurs qui vont de 1 à 10 (`[i for i in range(1, 11)]`)

    !!! faq "Question 4" 
        === "Question"
            Quelles sont les valeurs des cartes violettes ?

        === "Solution"
            - Les cartes 10 cartes violettes ont pour valeur 10, 30, 50, 70, 90 (`[ i*10 for i in vertes if i%2 != 0]`)
            - Chaque carte violette est en double dans le jeu (`violettes = violettes * 2`)

    !!! faq "Question 5" 
        === "Question"
            Comment est représenté le 5 Vert ? Par quelle strucuture de données ?

        === "Solution"
            - Le 5 vert est représenté par un tuple
            - Le tuple `(1, 5)`

    !!! faq "Question 6" 
        === "Question"
            Donner le premier élément et le dernier élément de la liste retournée par la fonction `crea_jeu()`
            
        === "Solution"
            - Le premier élément est la carte Vert 1 : `(1, 1)`
            - Le dernier élémentt est une carte Orange : : `(2, 0)`