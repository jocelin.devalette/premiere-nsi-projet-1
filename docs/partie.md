# La Partie de Cartes

## Distribution des cartes

!!! info "la fonction `distribue(jeu)`"
    Le jeu de cartes est distribué aléatoirement entre les deux joueurs. On constitue ainsi deux jeux de cartes qui sont les listes `jeu1` et `jeu2` qui correspondent aux jeux de chacun des joueurs. 
    
    La fonction `distribue(jeu)` renvoie un tuple contenant les deux listes `jeu1` pour le joueur 1 et `jeu2` pour le joueur 2 :

    ```python linenums="1"
    def distribue(jeu) :
        jeu1 , jeu2 = [], []
        while len(jeu) != 0 :
            a = randint(0, len (jeu) - 1)
            jeu1.append(jeu[a])
            del jeu[a]
            b = randint(0, len(jeu) - 1 )
            jeu2.append(jeu[b])
            del jeu[b]
        return jeu1, jeu2
    ```

!!! note "Répondez aux questions"
    Essayez, en analysant la fonction `distribue(jeu)`, de répondre aux questions ci-dessous :

    !!! faq "Question 1" 
        === "Question"
            Donner une pré-condition sur la liste `jeu` passée en paramètre de la fonction `distribue(jeu)`

        === "Solution"
            La liste `jeu` doit contenir un nombre pair d'éléments. Il est possible de passer une liste vide en argument. `jeu1` et  `jeu2` seront dans ce cas des listes vides.

    !!! faq "Question 2" 
        === "Question"
            A quelle ligne se produira l'erreur lors de l'éxécution de la fonction `distribue(jeu)` si la précondition établie à la question 1 n'est pas respectée ?

        === "Solution"
            L'erreur se produira à la ligne 7 lors de l'appel de `randint(0, -1)`, puisque à ce moment là la liste `jeu` est une liste vide.

    !!! faq "Question 3" 
        === "Question"
            Quelle est le format de la donnée retournée par la fonction `distribue(jeu)`

        === "Solution"
            La fonction retourne un **tuple** de deux éléments, `jeu1` et `jeu2`.

    !!! tip "Remarque"
        Les deux liste renvoyées ne sont donc pas classées et contiennent 12 cartes chacune (si le jeu contient bien 24 cartes), par exemple on peut avoir :

        ```python
        [[0, 70], [1, 8], [1, 7], [2, 0], [0, 30], ...]
        ```

## Gagner le pli

!!! faq "la fonction `pli(carte1, carte2`"
    Compléter la une fonction ci dessous`pli(carte1,carte2)` pour qu'elle renvoie la carte qui gagne le pli.

    - Données : carte1 et carte2 sont des tuples de la forme `(couleur,valeur)`.
    - Résultat : retourne sous forme de liste `[couleur,valeur]` la carte gagnant le pli. Retourne `None` en cas d'égalité

    ??? "A vous de jouer"
        === "La fonction pli(carte1,carte2)"
           
            {{IDE('pli')}}


## Une stratégie aléatoire

!!! info "La stratégie de la petite nièce"
    Pour commencer, nous allons faire jouer notre petite nièce, qui n'a pas très bien compris les régles du jeu et pour jouer choisit une carte au hasard dans sa main. Vous trouverez ci-dessous la fonction qui met en oeuvre 'la stratégie' de la petite nièce :

    ```python
        def jouer_carte1(main) :
            """ main : la liste non vide de cartes représentant le jeu de la petite nièce 
            la fonction retourne la carte jouée """
            a = randint(0, len(main) - 1)
            carte = main[a]
            return carte
    ```

## La stratégie du robot

!!! info "Une stratégie plus élaborée"
    Le programme joue lui selon une stratégie plus élaborée, il ne joue pas ses cartes n'importe comment. Il suit une logique définie par un algorithme, qui est traduit par la fonction en python ci-dessous :

    ```python
        def jouer_carte2(main) :
            """ main est une liste de cartes que peut jouer le programme
            la fonction retourne la carte jouée"""
            liste1 = [i[1] for i in main if i[0]==1]
            liste2 = [i[1] for i in main if i[0]==0]
            for i in range(len(main)) :
                if main[i] == 2 :
                    carte = main[i]
                    return carte
            for i in range(len(main)) :
                if main[i][0] == 1 and main[i][1] == max(liste1) :
                    carte = main[i]
                    return carte
            for i in range(len(main)) :
                if main[i][0] == 0 and main[i][1] == min(liste2) :
                    carte = main[i]
                    return carte
            carte = main[0]
            return carte
    ```

