## Espace de tests pour vos scripts

!!! info "pour tester vos scripts"
    Cette page vous permettra de tester vos scripts de qualidfication pour la phase de qualification. Il vous suffira pour cela :
    
    - de modifier la fonction `jouer_carte1(main)`
    - d'éxécuter le script 
    - d'appeler dans la console `parties(n)`, avec un $n<10000$ pour éviter les temps d'éxécution trop longs.

    {{IDE('tournoi')}}



