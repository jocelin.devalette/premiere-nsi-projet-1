# Travail à réaliser

## Phase de qualification

!!! danger "Objectif n°1 : Battre le programme"

    Dans cette première phase du projet, vous aller devoir écrire votre propre fonction `jouer_carte1(main)` et remplacer la stratégie de la petite nièce par votre propre stratégie.

    Vous devez obtenir un pourcentage de victoires **supérieur à 90%** pour accéder à la phase suivante, le tournoi.

    Dans cette première phase la fonction `jouer_carte1(main)` n'accepte qu'un seul paramètre : la main du joueur, c'est à dire la liste des cartes à disposition du joueur.

    ```python
    def jouer_carte1(main) :
        """ choisit la carte à jouer dans la liste main
        renvoie la carte jouée """
        ....
        ....
        return carte
    ```

## Le Tournoi.

!!! danger "Objectif n°2 : Battre les programmes des autres joueurs"
    Dans cette seconde phase, vous allez jouer contre les algorithmes d'autres joueurs. La fonction `jouer_carte` va donc se compliqué un peu, en effet elle accepte maintenant 2 paramètres, d'une part votre main, mais aussi la liste des cartes déjà jouées.

    Vous avec donc la possibilité de connaitre les cartes que votre adersaire a en main et donc affinez votre stratégie de jeu.

    Proposez une fonction `jouer_carte`

    ```python
    def jouer_carte(main,jouees) :
        """ choisit la carte à jouer dans la liste main
        main : liste de cartes que le joueur peut jouer
        jouees : liste des cartes déjà jouées
        renvoie la carte jouée """
        ....
        ....
        return carte
    ```
!!! info "le déroulement du tournoi"

    A l'issue de la phase de qualification, les joueurs sont classés sur la base du score réalisé contre le programme.

    Les huits premiers joueurs de la phase de qualifications sont sélectionnés pour participer au **Grand Tournoi** qui se déroulera selon le schéma suivant :

    ``` mermaid
    flowchart TB
        J1 --> Q1
        J8 --> Q1
        J4 --> Q2
        J5 --> Q2
        J3 --> Q3
        J6 --> Q3
        J2 --> Q4
        J7 --> Q4
        Q1 --> Q5
        Q2 --> Q5
        Q3 --> Q6
        Q4 --> Q6
        Q5 --> V
        Q6 --> V
    ```

## Déposer vos fonctions.

!!! note "Instructions" 
    Vous déposerez vos deux fonctions sur Labo-Sciences. Les fichiers seront commentés et vos fonction spécifiées. Chaque fichier sera au format `*.py`.

    - fonction n°1 : Qualification, le fichier sera nommé `qualif.py`
    - fonction n°2 : Grand tournoi, le fichier sera nommé `tournoi.py`


    [Dépôts de fichiers](https://labo-sciences.fr/moodle/mod/assign/view.php?id=1499){ .md-button .md-button--primary}
    

