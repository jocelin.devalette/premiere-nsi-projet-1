def pli (carte1, carte2) :
    """ carte1 et carte2 : tuples de la forme (Couleur, Valeur) """
    """ retourne un tuple : carte ayant remporte le pli """
    """ retourne None en cas d'egalite """
    if carte1[0] > carte2[0] : # On vérifie la couleur des cartes
        return carte1
    elif carte1[0] < carte2[0] :
        return carte2
    else :
        if carte1[1] > carte2[1] : # si les couleurs sont identiques, on vérifie leur valeur
            return carte1
        elif carte1[1] < carte2[1] :
            return carte2
    return None # si les cartes sont identiques on retourne None