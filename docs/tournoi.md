# Le Grand Tournoi

## Déroulement de la partie

!!! tip "Une fonction `partie`"
    On dispose maintenant des fonctions :

    - `crea_jeu()` : génére le jeu
    - `distribue(jeu)` : distribue le jeu de carte, retourne deux listes de cartes pour chacun des joueurs.
    - `pli(carte1, carte2)` : Détermine la carte gagnante du pli ou renvoie `None` en cas d'égalité.
    - `jouer_carte1(main)` : Choisit une carte à jouer dans main du joueur 1, avec pour le moment une stratégie *petite nièce*.
    - `jouer_carte2(main)` : Choisit une carte à jouer dans main du joueur 2.

    ```python linenums="1"
        def partie() :
            joueur1, joueur2 = distribue(crea_jeu())
            score1, score2 = 0, 0
            while len(joueur1) != 0 and len(joueur2) != 0 :
                carte_j1 = jouer_carte1(joueur1)
                joueur1.remove(carte_j1)
                carte_j2 = jouer_carte2(joueur2)
                joueur2.remove(carte_j2)
                gagnant = pli(carte_j1, carte_j2)
                if gagnant == carte_j1 :
                    score1 = score1 + carte_j1[1] + carte_j2[1]
                elif gagnant == carte_j2 :
                    score2 = score2 + carte_j1[1] + carte_j2[1] 
            if score1 > score2 :
                return 1
            elif score1 < score2 :
                return 2
            else :
                return 0
    ```

    !!! note "Répondez aux questions"
        Quelques questions pour voir si vous comprennez bien comment focntionne la fonction `partie()`

        !!! faq "Question 1" 
            === "Question"
                A quelle condition l'instruction `while` de la ligne **4** pourrait conduire à une boucle infinie:

            === "Solution"
                Les listes `joueur1` et `joueur2` doivent être de même longueur.

        !!! faq "Question 2" 
            === "Question"
                Comment sait-on à l'issue de l'éxécution de la focntion `partie()` que le joueur 2 a gagné ?

            === "Solution"
                La fonction retourne un entier **2**.

        !!! faq "Question 3" 
            === "Question"
                De quelle type est la variable `score1`

            === "Solution"
                `score1` est un entier (int).

## Efficacité des algorithmes de jeu.

Pour éviter que le hasard guide le jeu, nous allons maintenant faire jouer 10 000 parties consécutives. La fonction doit renvoyer le pourcentage de victoire de chaque joueur.

!!! tip "L'affrontement"

    ```python
        def parties(n) :
            g1, g2 = 0,0
            for i in range (n) :
                resultat = partie()
                if resultat == 1 :
                    g1 += 1
                elif resultat == 2 :
                    g2 += 1
            return g1/n*100, g2/n*100
    ```

    Quelques exemples de résultats de parties, entre la stratégie de la petite nièce( enfin si l'on peut vraiment parler de stratégie) et la stratégie du programme.

    ```python
        >>> parties(100)
        (56.0, 44.0)
        >>> parties(1000) 
        (51.2, 48.8)
        >>> parties(10000)
        (49.36, 50.64)
        >>> parties(100000)
        (49.934, 50.066)

    ```
    On se rend compte assez vite que la stratégie de la petite nièce n'est pas vraiment efficace contre l'algorithme du programme. Il semblerait que la petite niéce en appliquant sa stratégie aléatoire gagne environ 1 partie sur 2.
